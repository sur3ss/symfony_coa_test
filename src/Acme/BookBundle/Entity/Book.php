<?php

namespace Acme\BookBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Book
 *
 * @ORM\Table(name="book")
 * @ORM\Entity
 */
class Book {

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var string $name
     * 
     * @ORM\Column(name="name", type="string", length=255, unique=false, nullable=false)
     */
    protected $name;

    /**
     *
     * @var string $price
     * 
     * @ORM\Column(name="price", type="decimal", precision=9, scale=2, unique=false, nullable=false)
     */
    protected $price;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection $authors
     *
     * @ORM\ManyToMany(targetEntity = "Acme\BookBundle\Entity\Author", mappedBy = "books")
     */
    protected $authors;

    /**
     * Get id.
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->authors = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @return string
     */
    public function __toString() {
        return $this->getName();
    }

    /**
     * Set name.
     *
     * @param string $name
     * @return Book
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price.
     *
     * @param string $price
     * @return Book
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Add author.
     *
     * @param \Acme\BookBundle\Entity\Author $author
     * @return Book
     */
    public function addAuthor(\Acme\BookBundle\Entity\Author $author)
    {
        $this->authors[] = $author;

        return $this;
    }

    /**
     * Remove authors.
     *
     * @param \Acme\BookBundle\Entity\Author $authors
     */
    public function removeAuthor(\Acme\BookBundle\Entity\Author $authors)
    {
        $this->authors->removeElement($authors);
    }

    /**
     * Get authors.
     *
     * @return \Doctrine\Common\Collections\ArrayCollection 
     */
    public function getAuthors()
    {
        return $this->authors;
    }
}
